﻿using SimpleUi.Abstracts;
using UnityEngine.UI;
using VContainer;

namespace Example.Views
{
	public class FirstView : UiView
	{
		public Button Next;
		public Button Back; 
		public ElementCollection Elements;

		protected override void Resolve(IObjectResolver resolver)
		{
			resolver.Inject(Elements);
		}
	}
}