﻿using SimpleUi;
using VContainer.Unity;

namespace Example
{
	public class TestManager : IPostInitializable
	{
		private readonly ILogger _logger;

		public TestManager(
			ILogger logger
		)
		{
			_logger = logger;
		}

		public void PostInitialize()
		{
			Ui.OpenWindow("First");
			_logger.Log("Open First");
		}
	}
}