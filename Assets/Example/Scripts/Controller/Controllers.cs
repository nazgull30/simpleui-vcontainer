﻿using Example.Views;
using SimpleUi;
using SimpleUi.Abstracts;
using UniRx;
using VContainer.Unity;

namespace Example.Controller
{
	public class FirstController : UiController<FirstView>, IInitializable
	{
		// [Inject] public IObjectResolver _resolve;
		
		public void Initialize()
		{
			// _resolve.Inject(View.Elements);
			
			View.Next.OnClickAsObservable()
				.Subscribe(_ => Ui.OpenWindow<SecondWindow>())
				.AddTo(View.Next);
			View.Back.OnClickAsObservable().Subscribe(_ =>  Ui.Back()).AddTo(View.Back);

			for (var i = 0; i < 5; i++)
			{
				var element = View.Elements.Create();
				element.textTitle.text = $"Title {i}";
			}
		}
	}

	public class SecondController : UiController<SecondView>, IInitializable
	{
		public void Initialize()
		{
			View.Next.OnClickAsObservable()
				.Subscribe(_ => Ui.OpenWindow<ThirdWindow>())
				.AddTo(View.Next);
			View.Back.OnClickAsObservable().Subscribe(_ => Ui.Back()).AddTo(View.Back);
		}
	}

	public class ThirdController : UiController<ThirdView>, IInitializable
	{
		public void Initialize()
		{
			View.Next.OnClickAsObservable()
				.Subscribe(_ =>  Ui.OpenWindow<FirstPopUpWindow>())
				.AddTo(View.Next);
			View.Back.OnClickAsObservable().Subscribe(_ =>  Ui.Back()).AddTo(View.Back);
		}
	}

	public class FirstPopUpController : UiController<FirstPopUpView>, IInitializable
	{
		public void Initialize()
		{
			View.Next.OnClickAsObservable()
				.Subscribe(_ => Ui.OpenWindow<SecondPopUpWindow>())
				.AddTo(View.Next);
			View.Back.OnClickAsObservable().Subscribe(_ => Ui.Back()).AddTo(View.Back);
		}
	}

	public class SecondPopUpController : UiController<SecondPopUpView>, IInitializable
	{
		public void Initialize()
		{
			View.Next.OnClickAsObservable()
				.Subscribe(_ => Ui.OpenWindow<FourthWindow>())
				.AddTo(View.Next);
			View.Back.OnClickAsObservable().Subscribe(_ => Ui.Back()).AddTo(View.Back);
		}
	}

	public class FourthController : UiController<FourthView>, IInitializable
	{
		public void Initialize()
		{
			View.Next.OnClickAsObservable()
				.Subscribe(_ => Ui.OpenWindow<FifthWindow>())
				.AddTo(View.Next);
			View.Back.OnClickAsObservable().Subscribe(_ => Ui.Back()).AddTo(View.Back);
		}
	}

	public class FifthController : UiController<FifthView>, IInitializable
	{
		public void Initialize()
		{
			View.Back.OnClickAsObservable().Subscribe(_ => Ui.Back()).AddTo(View.Back);
		}
	}
}