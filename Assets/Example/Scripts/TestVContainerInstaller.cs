using VContainer;
using VContainer.Unity;

namespace Example
{
	public class TestVContainerInstaller : MonoInstaller
	{
		public override void Install(IContainerBuilder builder)
		{
			builder.RegisterEntryPoint<FirstLoggerEntryPoint>();
			builder.RegisterEntryPoint<SecondLoggerEntryPoint>();

			builder.Register<FirstLogger>(Lifetime.Singleton).AsImplementedInterfaces();

			builder.Register<SecondLogger>(Lifetime.Singleton).AsImplementedInterfaces()
				.WhenInjectedInto<SecondLoggerEntryPoint>();
		}
	}
}