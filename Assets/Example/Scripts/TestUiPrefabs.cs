﻿using Example.Controller;
using Example.Views;
using SimpleUi;
using SimpleUi.Interfaces;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Example
{
	[CreateAssetMenu(menuName = "Settings/TestUiPrefabs", fileName = "TestUiPrefabs")]
	public class TestUiPrefabs : ScriptableObjectInstaller
	{
		[SerializeField] private Canvas _canvas;
		[SerializeField] private FirstView _firstView;
		[SerializeField] private SecondView _secondView;
		[SerializeField] private ThirdView _thirdView;
		[SerializeField] private FourthView _fourthView;
		[SerializeField] private FifthView _fifthView;
		[SerializeField] private FirstPopUpView _firstPopUpView;
		[SerializeField] private SecondPopUpView _secondPopUpView;

		public override void Install(IContainerBuilder builder)
		{
			var canvas = Instantiate(_canvas);
			var parent = canvas.transform;
			builder.RegisterFromComponent<CustomGraphicRaycaster>(canvas.gameObject, Lifetime.Singleton)
				.As<IUiFilter>();
			builder.BindUiView<FirstController, FirstView>(_firstView, parent);
			builder.BindUiView<SecondController, SecondView>(_secondView, parent);
			builder.BindUiView<ThirdController, ThirdView>(_thirdView, parent);
			builder.BindUiView<FourthController, FourthView>(_fourthView, parent);
			builder.BindUiView<FifthController, FifthView>(_fifthView, parent);
			builder.BindUiView<FirstPopUpController, FirstPopUpView>(_firstPopUpView, parent);
			builder.BindUiView<SecondPopUpController, SecondPopUpView>(_secondPopUpView, parent);
		}
	}
}