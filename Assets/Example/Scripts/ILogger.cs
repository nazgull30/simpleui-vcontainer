using UnityEngine;
using VContainer.Unity;

namespace Example
{
	public interface ILogger
	{
		void Log(string log);
	}

	public class FirstLogger : ILogger
	{
		public void Log(string log)
		{
			Debug.Log($"FirstLogger -> {log}");
		}
	}
	
	public class SecondLogger : ILogger
	{
		public void Log(string log)
		{
			Debug.Log($"SecondLogger -> {log}");
		}
	}
	
	public class FirstLoggerEntryPoint : IInitializable
	{
		private readonly ILogger _logger;

		public FirstLoggerEntryPoint(ILogger logger)
		{
			_logger = logger;
		}

		public void Initialize()
		{
			_logger.Log($"Initialize FirstLoggerEntryPoint");
		}
	}
	
	
	public class SecondLoggerEntryPoint : IInitializable
	{
		private readonly ILogger _logger;

		public SecondLoggerEntryPoint(ILogger logger)
		{
			_logger = logger;
		}

		public void Initialize()
		{
			_logger.Log("Initialize SecondLoggerEntryPoint");
		}
	}
}