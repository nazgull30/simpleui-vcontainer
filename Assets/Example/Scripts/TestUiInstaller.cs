﻿using EventBus.Impls;
using SimpleUi;
using SimpleUi.Managers;
using SimpleUi.Signals;
using VContainer;
using VContainer.Unity;

namespace Example
{
	public class TestUiInstaller : MonoInstaller
	{
		public override void Install(IContainerBuilder builder)
		{
			BindWindowSignalBus(builder);
			
			builder.Register<FirstWindow>(Lifetime.Singleton).AsImplementedInterfaces().AsSelf();
			builder.Register<SecondWindow>(Lifetime.Singleton).AsImplementedInterfaces().AsSelf();
			builder.Register<ThirdWindow>(Lifetime.Singleton).AsImplementedInterfaces().AsSelf();
			builder.Register<FourthWindow>(Lifetime.Singleton).AsImplementedInterfaces().AsSelf();
			builder.Register<FifthWindow>(Lifetime.Singleton).AsImplementedInterfaces().AsSelf();
			builder.Register<FirstPopUpWindow>(Lifetime.Singleton).AsImplementedInterfaces().AsSelf();
			builder.Register<SecondPopUpWindow>(Lifetime.Singleton).AsImplementedInterfaces().AsSelf();
			
			builder.Register<UiFilterManager>(Lifetime.Singleton).AsImplementedInterfaces().AsSelf();
			builder.RegisterEntryPoint<TestManager>();
			
			builder.BindWindowsController<WindowsController>(EWindowLayer.Local);
		}

		private void BindWindowSignalBus(IContainerBuilder builder)
		{
			EventsHolder.RegisterEvent<SignalActiveWindow>();
			EventsHolder.RegisterEvent<SignalBackWindow>();
			EventsHolder.RegisterEvent<SignalCloseWindow>();
			EventsHolder.RegisterEvent<SignalFocusWindow>();
			EventsHolder.RegisterEvent<SignalOpenRootWindow>();
			EventsHolder.RegisterEvent<SignalOpenWindow>();
			EventsHolder.RegisterEvent<SignalShowWindow>();
		}
	}
}