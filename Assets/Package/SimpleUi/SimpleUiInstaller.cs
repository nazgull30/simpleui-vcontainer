using SimpleUi.Signals;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace SimpleUi
{
	public class SimpleUiInstaller : MonoInstaller
	{
		[SerializeField] private EWindowLayer windowLayer;

		public override void Install(IContainerBuilder builder)
		{
			builder.BindWindowsController<WindowsController>(windowLayer);
		}
	}
}
